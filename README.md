# shell/theia-tools

shell tools for using [theia](https://theia-ide.org/)

status: alpha


###### Table of Contents

- [Requirements](#Requirements)
- [Usage](#Usage)
- [License](#License)


## Requirements

- sh


## Usage

### download-vsix-from-marketplace.sh

```bash
download-vsix-from-marketplace.sh /path/to/download ${PUBLISHER}.${NAME} ${VERSION}
# => /path/to/download/${PUBLISHER}.${NAME}/${VERSION}.vsix
```


## License

[MIT](LICENSE) license.

Copyright &copy; shun-fix9
