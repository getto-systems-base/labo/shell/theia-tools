#!/usr/bin/env sh

if [ $# -lt 3 ]; then
  echo "usage: download-vsix-from-vsassets.sh <path/to/download> <PUBLISHER>.<NAME> <VERSION>"
  exit 1
fi

download=$1

package=$2
publisher=${package%.*}
name=${package#*.}

version=$3

mkdir -p "${download}/${publisher}.${name}"

echo "download ${publisher}.${name}:${version} to ${download}"
curl -sSL "https://${publisher}.gallery.vsassets.io/_apis/public/gallery/publisher/${publisher}/extension/${name}/${version}/assetbyname/Microsoft.VisualStudio.Services.VSIXPackage" -o ${download}/${publisher}.${name}/${version}.vsix
